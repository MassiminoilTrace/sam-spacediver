# Sam Spacediver
Videogioco per il corso di Realtà virtuale del Politecnico di Torino


## Autori in ordine alfabetico:
### Our copyright notice:
©2020 Fabio Pellone, Leonardo Vezzani Massimo Gismondi, Mara Lupano, Simona Potenza

### Other copyright notices of software and music used in the project:
- Unity DoTween plugin → ©2015 Daniele Giardini, Demigiant. [Source code](https://github.com/Demigiant/dotween)
- Music → "Arecibo Message" by Jesse Spillane licensed under a Attribution License CC BY. [Artist's page](https://freemusicarchive.org/music/Jesse_Spillane/)

## Download builds
Download the windows and linux builds at [this link](https://vezzani.itch.io/samspacediver)