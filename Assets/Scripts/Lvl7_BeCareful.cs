﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lvl7_BeCareful : MonoBehaviour
{
    private bool Attivo = false;
    [SerializeField] Image PressTheButton;

    void Start()
    {
        this.GetComponent<Image>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PressTheButton.enabled && !Attivo)
        {
            this.GetComponent<Image>().enabled = true;
            Attivo = true;

        }
        if (Attivo)
        {
            if (Input.GetButtonDown("PulsanteGravDestra") || Input.GetButtonDown("PulsanteGravSinistra"))
            {
                this.GetComponent<Image>().enabled = false;
            }
        }
    }

  
}
