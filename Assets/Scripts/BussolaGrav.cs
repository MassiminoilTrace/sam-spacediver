﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BussolaGrav : MonoBehaviour
{
    [SerializeField] Transform freccia;
    [SerializeField] GestoreGravita gg; // messa in relazione al gestore gravità, così che sia legata al motore gravitazionale in qualche modo



    void Update()
    {
        freccia.transform.rotation = gg.transform.rotation;
        freccia.transform.up = - gg.transform.up;
    }
}
