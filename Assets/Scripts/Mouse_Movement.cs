﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouse_Movement : MonoBehaviour
{
    // DA POSIZIONARE SUL CANVAS_MOOUSE

    private float waitTime = 1.0f;
    private float timer = 0.0f;
    private bool startTimer = true;
    private GameObject cameraInterpolata;
    private Transform player;

    Vector3 mousePosition;
    Vector3 precedenteMousePosition;


    // Start is called before the first frame update
    void Start()
    {
        cameraInterpolata = GameObject.FindGameObjectWithTag("CameraInterpolata");
        this.GetComponent<Canvas>().enabled = false;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        mousePosition = Input.mousePosition;
        
    }
    
    // Update is called once per frame
    void Update()
    {
        if (startTimer)
        {
            timer += Time.deltaTime;
        }
        precedenteMousePosition = mousePosition;
        mousePosition = Input.mousePosition;
        
        if (startTimer && timer >= waitTime)
        {
            startTimer = false;
            this.GetComponent<Canvas>().enabled = true;
            this.GetComponent<Canvas>().transform.position = player.position;// + Physics.gravity.normalized * -1.5f;
            this.GetComponent<Canvas>().transform.rotation = cameraInterpolata.transform.rotation;
            

        }
        if (this.GetComponent<Canvas>().enabled && mousePosition!=precedenteMousePosition)
        {
            this.GetComponent<Canvas>().enabled = false;
            startTimer = false;
        }
        

        
    }
}
