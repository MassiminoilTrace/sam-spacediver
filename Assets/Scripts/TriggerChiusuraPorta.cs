﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChiusuraPorta : MonoBehaviour
{
    [SerializeField]
    private Porta p;
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Player")
        {
            return;
        }
        if (p==null)
        {
            return;
        }
        p.disattiva();
    }
}
