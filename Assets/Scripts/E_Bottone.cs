﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_Bottone : MonoBehaviour
{
    // DA POSIZIONARE SUL BOTTONE E PASSARE L' E_CANVAS_BOTTONE

    [SerializeField]
    private Canvas canvas;
    private GameObject cameraInterpolata;
    private Transform player;
    public float pickUpDist;
    // Start is called before the first frame update
    void Start()
    {
        cameraInterpolata = GameObject.FindGameObjectWithTag("CameraInterpolata");
        canvas.enabled = false;
        player = null;
        pickUpDist = 3f;
    }

    
    // Update is called once per frame
    void Update()
    {
            Collider[] pickups = Physics.OverlapSphere(transform.position, pickUpDist);
            // Find the closest
            foreach (Collider v in pickups)
            {
                if (v.gameObject.tag == "Player")
                {
                    player = v.transform;
                }
            }

            if (player != null)
            {
                canvas.enabled = true;
                player = null;
            }
            else if (canvas.enabled)
            {
                canvas.enabled = false;
            }
        
        if (canvas.enabled)
        {
            //canvas.transform.position = transform.position + Physics.gravity.normalized * -1.5f;
            //canvas.transform.rotation = cameraInterpolata.transform.rotation;
        }
    }
}
