﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_Cubo : MonoBehaviour
{
    // DA POSIZIONARE SUL CUBO E PASSARE L' E_CANVAS_CUBO

    [SerializeField]
    private Canvas canvas;
    private GameObject cameraInterpolata;
    private GameObject sam;
    private bool isGrabbed;
    public float pickUpDist;
    private bool canvasAttivo;
    bool afferrato = false;
    private bool suggerente;

    private float waitTime = 3.0f;
    private float timer = 0.0f;

    //AUDIO
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip[] impacts;

    // Start is called before the first frame update
    void Start()
    {
        cameraInterpolata = GameObject.FindGameObjectWithTag("CameraInterpolata");
        if (canvas != null) canvas.enabled = false;
        isGrabbed = false;
        pickUpDist = 1.5f;
        suggerente = false;
        sam = GameObject.FindGameObjectWithTag("Player");

        //da CuboAfferrato.cs
        canvasAttivo = false;

        //AUDIO
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        /*timer += Time.deltaTime;
        if (Input.anyKey)
        {
            timer = 0.0f;
        }

        if (timer >= waitTime && isGrabbed)
        {
            canvasAttivo = !canvasAttivo;
            if (canvasAttivo != canvas.enabled) canvas.enabled = !canvas.enabled;

        }*/
        
        if (canvasAttivo)
        {
            canvas.transform.position = transform.position + Physics.gravity.normalized * -1.5f;
            canvas.transform.rotation = cameraInterpolata.transform.rotation;
        }
    }

    public void IsGrabbed()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        ShowCanvas();
        isGrabbed = true;
        if (sam.GetComponent<Afferrare_2>().MostraSugg()) StartCoroutine(SuggDrop());
    }

    public void NotGrabbed()
    {
        isGrabbed = false;
        if (suggerente)
        {
            ShowCanvas();
            suggerente = false;
        }
        //controllo e show
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        //AUDIO
        if (collision.collider.transform.gameObject != GameObject.FindGameObjectWithTag("Player")) audioSource.PlayOneShot(impacts[Random.Range(0,5)], collision.impulse.magnitude / 10);

        //da CuboAfferrato.cs
        if (afferrato) transform.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }

    private void OnCollisionStay(Collision collision)
    {

        if (collision.impulse.magnitude > 3)
        {
            audioSource.PlayOneShot(impacts[Random.Range(0, 5)], collision.impulse.magnitude / 15);
        }
    }

    public void ShowCanvas()
    {
        if (!isGrabbed || suggerente)
        {
            canvasAttivo = !canvasAttivo;
            if (canvasAttivo)
            {
                canvas.transform.position = transform.position + Physics.gravity.normalized * -1.5f;
                canvas.transform.rotation = cameraInterpolata.transform.rotation;
            }
            if (canvasAttivo != canvas.enabled) canvas.enabled = !canvas.enabled;
        }
    }


    //da CuboAfferrato.cs
    public void SetAfferrato(bool boolean)
    {
        afferrato = boolean;
    }

    IEnumerator SuggDrop()
    {
        yield return new WaitForSecondsRealtime(5f);
        if (isGrabbed && sam.GetComponent<Afferrare_2>().MostraSugg())
        {
            suggerente = true;
            ShowCanvas();
        }
    }
}
