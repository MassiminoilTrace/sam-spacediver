﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump_UI : MonoBehaviour
{
    //DA POSIZIONARE SUL JUMP_CANVAS
    float Dist = 7f;
    Collider player;
    int space = 0;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Canvas>().enabled = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] pickups = Physics.OverlapSphere(transform.position, Dist);
        
        foreach (Collider v in pickups)
        {
            
            if (space==0 && v == player)
            {
                this.GetComponent<Canvas>().enabled = true;
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            this.GetComponent<Canvas>().enabled = false;
            space++;
        }
    }
}
