﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Bussola : MonoBehaviour
{
    //DA POSIZIONARE SUL CANVAS_BUSSOLA

    int changeGravity = 0;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("PulsanteGravDestra") || Input.GetButtonDown("PulsanteGravSinistra")) changeGravity++;
        if (changeGravity == 1) Invoke("ShowUI",2f);
    }

    void ShowUI()
    {
        this.GetComponent<SpriteRenderer>().enabled = true;
        Invoke("HideUI", 5f);
        changeGravity++;
    }

    void HideUI()
    {
        this.GetComponent<SpriteRenderer>().enabled = false;
    }
}
