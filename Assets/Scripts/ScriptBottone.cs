﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBottone : MonoBehaviour
{
    [SerializeField]
    private Texture colore_pulsante_da_attivare;
    [SerializeField]
    private Texture colore_pulsante_attivato;




    private Collider colliderSam;
    [SerializeField] List<Light> luci;

    public Porta portaDaAprire;

    private bool vicino = false; //aggiungere canvas 

    private Vector3 posizioneSu; //movimento bottone 
    private Vector3 posizioneGiu; //movimento bottone 

    private bool giaAperta = false; //impedisce di premere il pulsante più volte

    private AudioSource audioSource;
    [SerializeField]
    private AudioClip nope;

    [SerializeField]
    private Canvas canvas; //aggiunta canva 



    // Start is called before the first frame update
    void Start()
    {
        colliderSam = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>();
        audioSource = transform.GetChild(0).GetComponent<AudioSource>();
        canvas.enabled = false;

        GameObject bottone = this.transform.GetChild(1).gameObject;
        this.posizioneSu = transform.InverseTransformPoint(bottone.transform.position);
        this.posizioneGiu = transform.InverseTransformPoint(bottone.transform.position) + new Vector3(0, -0.01f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (vicino == true && Input.GetKeyDown(KeyCode.Q))
        {
            if (giaAperta == false)
            {
                PressBottone();
                //movimento bottone 
                GameObject bottone = this.transform.GetChild(1).gameObject;
                bottone.transform.DOLocalMove(this.posizioneGiu, 0.3f, false);

                StartCoroutine(tornaSu()); //fine movimento del bottone su 

                portaDaAprire.attiva();
                giaAperta = true;

                canvas.enabled = false;
                this.colora();

                //Debug.Log("Premuto Bottone E");
                audioSource.Play();
            }
            else
            {
                audioSource.PlayOneShot(nope);
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            // Debug.Log("In trigger");
            SetVicino(true);
            //Debug.Log("Valore vicino: " + vicino);

            if (giaAperta == false)
            {
                canvas.enabled = true;
            }
        }
    }

    /*public void OnTriggerStay(Collider collider)
    {
        //Debug.Log("sono in trigger e questo è il collider: "+collider);
        if((collider == colliderSam)&& Input.GetKeyDown(KeyCode.E))
        {
            
            PressBottone();
            //animator.SetTrigger("bottone");
        }
        
    }*/

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {

            //Debug.Log("Valore vicino: " + vicino);
            //Debug.Log("Fuori trigger");
            SetVicino(false);
            //Debug.Log("Valore vicino: " + vicino);
            canvas.enabled = false;
        }
    }

    public void SetVicino(bool boolean)
    {
        vicino = boolean;
        //Debug.Log("cambio valore vicino");
    }

    public void PressBottone()
    {
        //Debug.Log("Premuto!");
        if (luci != null)
        {
            foreach (Light luce in luci)
            {
                luce.enabled = false;
            }
        }

        // Luce1.GetComponent<Light>().enabled = false;
        // Luce2.GetComponent<Light>().enabled = false;
        // Luce3.GetComponent<Light>().enabled = false;
        // Luce4.GetComponent<Light>().enabled = false;

    }

    IEnumerator tornaSu()
    {
        yield return new WaitForSeconds(1f);
        GameObject bottone = this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(this.posizioneSu, 0.2f, false);
    }



    private void colora()
    {
        GameObject bottone = this.transform.GetChild(1).gameObject;
        if (this.colore_pulsante_attivato != null && colore_pulsante_da_attivare != null)
        {
            Renderer m_Renderer = bottone.GetComponent<Renderer>();
            m_Renderer.material.SetTexture("_MainTex", colore_pulsante_attivato);
            m_Renderer.material.SetColor("_EmissionColor", new Color(12.0f / 255.0f, 201.0f / 255.0f, 6.0f / 255.0f));
        }

    }
}
