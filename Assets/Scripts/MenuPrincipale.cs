﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MenuPrincipale : MonoBehaviour
{
    [SerializeField]
    private GameObject pan;
    [SerializeField]
    private VideoPlayer vip;
    [SerializeField]
    private GameObject pannelloPrincipale;
    [SerializeField]
    private GameObject pannelloIstruzioni;
    [SerializeField]
    private GameObject pannelloCredits;

    private void Start()
    {
        if (GameObject.Find("Menu").GetComponent<Menu>().GetFinito()) Credits();
    }

    public void PlayVideo()
    {
        pan.SetActive(true);
        vip.Play();
        vip.GetComponent<Scene_Intro>().StartVideo();
        this.gameObject.SetActive(false);
    }

    public void Instructions()
    {
        pannelloPrincipale.SetActive(false);
        pannelloIstruzioni.SetActive(true);
    }

    public void Credits()
    {
        pannelloPrincipale.SetActive(false);
        pannelloCredits.SetActive(true);
    }

    public void Indietro(string daPan)
    {
        switch (daPan)
        {
            case "istruzioni":
                pannelloIstruzioni.SetActive(false);
                pannelloPrincipale.SetActive(true);
                break;
            case "credits":
                pannelloCredits.SetActive(false);
                pannelloPrincipale.SetActive(true);
                break;
        }
    }

    public void Esci()
    {
        Application.Quit();
    }

    IEnumerator RitardoAttivazione(GameObject go, float time)
    {
        yield return new WaitForSecondsRealtime(time);
        go.SetActive(true);
    }
}
