﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicheDiSottofondo : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource sorgente;
    private AudioLowPassFilter filtroPassaBasso;
    private Dictionary<string, AudioClip> file_audio = new Dictionary<string, AudioClip>();
    void Start()
    {
        filtroPassaBasso = gameObject.GetComponent<AudioLowPassFilter>();
        
        sorgente = gameObject.GetComponent<AudioSource>();


        DontDestroyOnLoad(transform.root.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void avviaRiproduzione()
    {
        if (sorgente.isPlaying)
        {
            return;
        }

        this.sorgente.Play(0);
    }

    public void fermaRiproduzione()
    {
        if (!sorgente.isPlaying)
        {
            return;
        }

        this.sorgente.Stop();
    }
    public void soloBassi(bool at)
    {
        if (at)
        {
            filtroPassaBasso.cutoffFrequency = 700;
        }
        else
        {
            filtroPassaBasso.cutoffFrequency = 22000;
        }
        
        
    }
}
