using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPCamera : MonoBehaviour
{                                     // Start is called before the first frame update
    public Transform Sam, head;
    private float mouseX, mouseY, precX;
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        precX = head.localEulerAngles.x;
    }

    // Verifico che se ci sia pressione del tasto di uscita
    // e rilascio cattura mouse
    void Update()
    {
        if (Input.GetButton("EsciDalGioco"))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void LateUpdate()
    {
        CameraMovement();
    }

    void CameraMovement()
    {}
}
