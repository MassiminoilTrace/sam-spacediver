﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Porta : OggettoInteragibile
{
    private int numAttivazioniAttuali;

    [SerializeField]
    private int NUMERO_INTERRUTTORI_DA_ATTIVARE;


    private Vector3 posizioneApertaDX;
    private Vector3 posizioneApertaSX;
    private Vector3 posizioneChiusaDX;
    private Vector3 posizioneChiusaSX;

    //AUDIO
    private AudioSource audioSource;

    private bool primoAvvio = true;

    //Le funzioni attiva e disattiva della porta vengono chiamate
    // da altri oggetti come PulsantePavimento e simili.
    [SerializeField]
    private bool aperta = false;

    void Start()
    {
        //parto da chiusa
        GameObject antaDX = this.transform.GetChild(1).gameObject;
        GameObject antaSX = this.transform.GetChild(2).gameObject;
        this.posizioneChiusaDX = transform.InverseTransformPoint(antaDX.transform.position);
        this.posizioneChiusaSX = transform.InverseTransformPoint(antaSX.transform.position);
        this.posizioneApertaDX = transform.InverseTransformPoint(antaDX.transform.position) + new Vector3(0, 0, -2);
        this.posizioneApertaSX = transform.InverseTransformPoint(antaSX.transform.position) + new Vector3(0, 0, 2);

        this.numAttivazioniAttuali = 0;

        //AUDIO
        audioSource = GetComponent<AudioSource>();

    }
    public override void commuta()
    {
        this.aperta = !this.aperta;
        if (this.aperta)
        {
            this.attiva();
        }
        else
        {
            this.disattiva();
        }
    }
    public override void attiva()
    {
        numAttivazioniAttuali++;
        Debug.Log("attiva porta, num: "+numAttivazioniAttuali);


        if (numAttivazioniAttuali == NUMERO_INTERRUTTORI_DA_ATTIVARE)
        {
            //Eseguo animazione per aprire la porta
            GameObject antaDX = this.transform.GetChild(1).gameObject;
            antaDX.transform.DOLocalMove(
                this.posizioneApertaDX,
                0.3f,
                false);

            GameObject antaSX = this.transform.GetChild(2).gameObject;
            antaSX.transform.DOLocalMove(
                this.posizioneApertaSX,
                0.3f,
                false);
            this.aperta = true;
            Debug.Log("Attiva");
            if (Time.timeScale != 0) AudioSource.PlayClipAtPoint(audioSource.clip, this.gameObject.transform.GetChild(0).transform.position);
        }

        numAttivazioniAttuali = Mathf.Clamp(this.numAttivazioniAttuali, 0, NUMERO_INTERRUTTORI_DA_ATTIVARE);

    }
    public override void disattiva()
    {
        Debug.Log("Inizio disattiva");

       
        numAttivazioniAttuali--;
        Debug.Log("disattiva porta, num: "+numAttivazioniAttuali);


        if (numAttivazioniAttuali < NUMERO_INTERRUTTORI_DA_ATTIVARE)
        {
            //Eseguo animazione per chiudere la porta
            GameObject antaDX = this.transform.GetChild(1).gameObject;
            antaDX.transform.DOLocalMove(
                this.posizioneChiusaDX,
                0.3f,
                false);
            GameObject antaSX = this.transform.GetChild(2).gameObject;
            antaSX.transform.DOLocalMove(
                this.posizioneChiusaSX,
                0.3f,
                false);
            this.aperta = false;
            //Debug.Log("Disattiva");
            if (Time.timeScale != 0)
            {
                AudioSource.PlayClipAtPoint(audioSource.clip, this.gameObject.transform.GetChild(0).transform.position);
            }

        }
        numAttivazioniAttuali = Mathf.Clamp(this.numAttivazioniAttuali, 0, NUMERO_INTERRUTTORI_DA_ATTIVARE);
    }

    void Update()
    {
        if (this.primoAvvio)
        {
            primoAvvio = false;
            if (aperta)
            {
                aperta = false;//Così l'animazione viene eseguita
                this.attiva();
            }
            else
            {
                aperta = true;//Così l'animazione viene eseguita
                this.disattiva();
            }

            numAttivazioniAttuali = Mathf.Clamp(this.numAttivazioniAttuali, 0, NUMERO_INTERRUTTORI_DA_ATTIVARE);
        }
    }
}
