﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lvl5_PressTheCube : MonoBehaviour
{
    private bool triggerAttivo = false;
    [SerializeField] Image PressTheButton;
    [SerializeField] GameObject Bottone;

    void Start()
    {
        this.GetComponent<Image>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (PressTheButton.enabled)
        {
            triggerAttivo = true;
            this.GetComponent<Image>().enabled = true;

        }
        if (Bottone.GetComponent<PulsantePavimento>().IsActivo())
        {
            this.GetComponent<Image>().enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggerAttivo)
        {
            this.GetComponent<Image>().enabled = false;
            
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (triggerAttivo)
        {
            this.GetComponent<Image>().enabled = true;
            
        }

    }
}
