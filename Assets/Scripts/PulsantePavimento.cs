﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PulsantePavimento : OggettoInteragibile
{
    private int vecchio_conteggio_oggetti_in_collisione = 0;
    private int conteggio_oggetti_in_collisione = 0;
    public Porta portaDaAprire;//L'oggetto Porta che verrà comandato da questo pulsante

    private bool isActive = false;
    //AUDIO
    private AudioSource audioSource;
    private Vector3 posizioneSu;
    private Vector3 posizioneGiu;

    private void Start()    //AUDIO
    {
        audioSource = GetComponent<AudioSource>();
        GameObject bottone = this.transform.GetChild(1).gameObject;
        this.posizioneSu = transform.InverseTransformPoint(bottone.transform.position);
        this.posizioneGiu = transform.InverseTransformPoint(bottone.transform.position) + new Vector3(0, -0.01f, 0);
        disattiva();
    }

    public bool IsActivo()
    {
        return isActive;
    }

    public override void commuta()
    {
        //Non serve, si può lasciare vuota
        if (portaDaAprire == null)
        {
            return;
        }
    }
    public override void attiva()
    {
        if (portaDaAprire == null)
        {
            return;
        }

        //movimento pulsante quando premuto
        GameObject bottone = this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneGiu,
            0.3f,
            false);

        //Comando la porta collegata a questo pulsante
        portaDaAprire.attiva();
        isActive = true;

        //AUDIO
        audioSource.Play();
    }
    public override void disattiva()
    {
        if (portaDaAprire == null)
        {
            return;
        }

        //Da inserire il comando per eseguire l'animazione
        // di ri-alzamento del pulsante
        GameObject bottone = this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneSu,
            0.3f,
            false);
        isActive = false;
        //Comando la porta collegata a questo pulsante
        portaDaAprire.disattiva();
    }

    private void aggiornaConteggio()
    {
        // Debug.Log("Vecchio conteggio: "+vecchio_conteggio_oggetti_in_collisione);
        // Debug.Log("Nuovo conteggio: "+conteggio_oggetti_in_collisione);
        if (
            conteggio_oggetti_in_collisione == 0
            &&
            vecchio_conteggio_oggetti_in_collisione > 0
        )
        {
            //Se prima c'erano oggetti e ora non più
            this.disattiva();
        }
        else if (
            conteggio_oggetti_in_collisione > 0
            &&
            vecchio_conteggio_oggetti_in_collisione == 0
        )
        {
            //Se ora ci sono oggetti sopra e prima no
            this.attiva();
        }
        vecchio_conteggio_oggetti_in_collisione = conteggio_oggetti_in_collisione;

    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Player" && collider.tag != "PickUp")
        {
            return;
        }
        this.conteggio_oggetti_in_collisione++;
        this.aggiornaConteggio();
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.tag != "Player" && collider.tag != "PickUp")
        {
            return;
        }
        this.conteggio_oggetti_in_collisione--;
        this.aggiornaConteggio();
    }

}
