﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class Raccolta_meteoriti : MonoBehaviour
{

    //private Text textCounter;
    public Transform sparkle;  
    //private bool preso;
    private static int IDCounter=0; 
    public int thisCoinID = -1;

    //AUDIO
    private AudioSource audioSource;

    void Reset(){
        thisCoinID = IDCounter;
        IDCounter++;
           // textCounter.text = IDCounter.ToString();
        }

    public static Dictionary<int, bool> coinCollectedDatabase;

    void Awake(){
        if (coinCollectedDatabase == null) coinCollectedDatabase = new Dictionary<int, bool>();

        if (GameObject.Find("Menu").GetComponent<Menu>().Riavviante())
        {
            coinCollectedDatabase = new Dictionary<int, bool>(GameObject.Find("Menu").GetComponent<Menu>().GetMeteoritiDB());
        }
        else
        {
            Invoke("SendCounter", 0.5f);
        }
    }

    void Start(){
        sparkle.GetComponent<ParticleSystem>().enableEmission=false;
        //textCounter = GameObject.FindGameObjectWithTag("textCounter").GetComponent<Text>();

        if (coinCollectedDatabase.ContainsKey(thisCoinID)){
            if(coinCollectedDatabase[thisCoinID]) Destroy(gameObject);
        }
        else {
            coinCollectedDatabase.Add(thisCoinID, false);
        }

        //AUDIO
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player") /*&& preso==false*/) {
           /* Debug.Log("Preso= " + preso);
            preso=true; 
            Debug.Log("Preso= " + preso);*/
            StartCoroutine(ScaleOverTime(1));
            sparkle.GetComponent<ParticleSystem>().enableEmission=true;
            StartCoroutine(stopSparkle());

            coinCollectedDatabase[thisCoinID] = true;
            Debug.Log(coinCollectedDatabase);
               // Reset();

            //AUDIO
            audioSource.Play();

            }
    }

    IEnumerator ScaleOverTime(float time) {
        Vector3 originalScale = transform.localScale;
        Vector3 destinationScale = new Vector3(0.01f,0.01f,0.01f);
         
        float currentTime = 0.0f;
         
        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);
         
        Destroy(gameObject);
    } 

    IEnumerator stopSparkle(){
        yield return new WaitForSeconds(100f);
        sparkle.GetComponent<ParticleSystem>().enableEmission=false;
    }

    private void SendCounter()
    {
        GameObject.Find("Menu").GetComponent<Menu>().SetMeteoriti(coinCollectedDatabase);
    }

}