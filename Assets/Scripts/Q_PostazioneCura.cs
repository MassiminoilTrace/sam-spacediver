﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Q_PostazioneCura : MonoBehaviour
{
    // DA POSIZIONARE SULLA POSTAZIONE DI CURA E PASSARLE IL Q_CANVAS


    [SerializeField]
    private Canvas canvas;
    
    private bool salito = false;
    // Start is called before the first frame update
    void Start()
    {
        canvas.enabled = false;
    }

    
    // Update is called once per frame
    void Update()
    {

        if (salito)
        {
            canvas.enabled = true;
        }
        else if (salito && Input.GetKeyDown(KeyCode.Q))
        {
            canvas.enabled = false;
        }else if (!salito)
        {
            canvas.enabled = false;
        }
        
        
    }

    public void SetSalito(bool boolean)
    {
        salito = boolean;
    }
}
