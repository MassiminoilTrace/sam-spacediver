﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostazioneCura : MonoBehaviour
{
    // DA POSIZIONARE SULLA POSTAZIONE DI CURA E PASSARE IL BOTTONE

    public Transform bottone;

    private Collider player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider == player)
        {
            //Debug.Log("player salito");
            transform.GetComponent<Q_PostazioneCura>().SetSalito(true);
            bottone.GetComponent<Bottone_Cura>().SetSalito(true);
        }

        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider == player)
        {
            //Debug.Log("player sceso");
            transform.GetComponent<Q_PostazioneCura>().SetSalito(false);
            bottone.GetComponent<Bottone_Cura>().SetSalito(false);
        }
    }

}
