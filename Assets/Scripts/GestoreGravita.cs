﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestoreGravita : MonoBehaviour
{
    //Gestione della gravità nel mondo
    //È sufficiente disporre un oggetto GestoreGravita all'interno della scena affinché abbia effetto
    //Esso va a modificare direttamente il vettore gravità del motore fisico di Unity
    public float normaEuclideaGravita = 9.8f;

    [SerializeField]
    private int indice_attuale = 0; //Indice per impostare la gravità iniziale usata al caricamento e avvio del livello
    Vector3[] v_gravita_possibili = {
        new Vector3(0, -1f, 0),
        new Vector3(0, 0, 1f),
        new Vector3(0, 1f, 0),
        new Vector3(0, 0, -1f)
        };  //Si usa un approccio con un array con valori già precalcolati anziché usare prodotti vettoriali, in maniera da evitare calcoli inutili nel girare la gravità

    void Start()
    {
        indice_attuale = indice_attuale % 4;
        Physics.gravity = v_gravita_possibili[indice_attuale] * normaEuclideaGravita;
    }


    public void giraOrario()
    {
        indice_attuale = (indice_attuale + 1) % 4;  //Verifico di evitare di andare fuori dalla grandezza array
        Physics.gravity = v_gravita_possibili[indice_attuale] * normaEuclideaGravita;
    }
    public void giraAntiOrario()
    {
        if (indice_attuale <= 0)  //Verifico di evitare di andare fuori dalla grandezza array
        {
            indice_attuale = 3;
        }
        else
        {
            indice_attuale = indice_attuale - 1;
        }
        Physics.gravity = v_gravita_possibili[indice_attuale] * normaEuclideaGravita;
    }
    public void giraSottoSopra()
    {
        indice_attuale = (indice_attuale + 2) % 4;  //Verifico di evitare di andare fuori dalla grandezza array
        Physics.gravity = v_gravita_possibili[indice_attuale] * normaEuclideaGravita;
    }
}
