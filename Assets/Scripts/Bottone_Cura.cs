﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bottone_Cura : MonoBehaviour
{
    //DA POSIZIONARE SUL BOTTONE DELLA POSTAZIONE DI CURA

    private GameObject player;
    private bool salito = false;

    private Vector3 posizioneSu;
    private Vector3 posizioneGiu;

    //AUDIO
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip good;
    [SerializeField]
    private AudioClip bad;

    //sparkle alla cura 
    public Transform sparkle; 

    // Start is called before the first frame update
    void Start()
    {
        sparkle.GetComponent<ParticleSystem>().enableEmission=false;

        player = GameObject.FindGameObjectWithTag("Player");

        //per il movimento del bottone 
        GameObject bottone= this.transform.GetChild(1).gameObject;
        this.posizioneSu= transform.InverseTransformPoint(bottone.transform.position);
        this.posizioneGiu= transform.InverseTransformPoint(bottone.transform.position) + new Vector3(0, -0.01f, 0);

        //AUDIO
        audioSource = transform.GetChild(0).GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (salito && Input.GetKeyDown(KeyCode.Q))
        {
            int meteoriti = player.GetComponent<CalcoloMeteoriti>().getNumber();
            //Debug.Log("meteoriti: " + meteoriti);
            if (meteoriti >= 3)
            {
                player.GetComponent<FallDamage>().Cura();

                //AUDIO
                audioSource.clip = good;
                audioSource.Play();
            
            //movimento bottone 
            GameObject bottone = this.transform.GetChild(1).gameObject;
            bottone.transform.DOLocalMove(
            this.posizioneGiu,
            0.3f,
            false);

            StartCoroutine(tornaSu());

            sparkle.GetComponent<ParticleSystem>().enableEmission=true;
            StartCoroutine(stopSparkle());

            }
            
            else
            {
                audioSource.PlayOneShot(bad);
            }

        }
        }

        IEnumerator tornaSu(){
            yield return new WaitForSeconds(1f);
            GameObject bottone= this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneSu, 
            0.3f,
            false);
        }

        IEnumerator stopSparkle(){
        yield return new WaitForSeconds(2f);
        sparkle.GetComponent<ParticleSystem>().enableEmission=false;
        }

    public void SetSalito(bool boolean)
    {
        salito = boolean;
    }
}
