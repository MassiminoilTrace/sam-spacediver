﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAperturaPorta : MonoBehaviour
{
    [SerializeField]
    private Porta p;

    [SerializeField] bool ApreNuovoLivello;
    [SerializeField] int NuovoLivello;
    private GameObject player;

    private void Start()
    {
        if (!ApreNuovoLivello) NuovoLivello = 0;
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("trigger porta");
        if (collider.tag != "Player")
        {
            return;
        }
        if (p==null)
        {
            return;
        }
        // Debug.Log("Porta aperta");
        p.attiva();
        if(ApreNuovoLivello)
        {
            player.GetComponent<Giocatore>().SetLivelloAttuale(NuovoLivello);
        }
    }
}
