﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalcoloMeteoriti : MonoBehaviour
{
    private int count;
    private Text textCounter;


    void Start()
    {
        count=0;
        // SetCountText();   
        textCounter = GameObject.FindGameObjectWithTag("textCounter").GetComponent<Text>();
    }

    void OnTriggerEnter(Collider other){
    if(other.gameObject.CompareTag("Meteorite")) {
     count= count +1;
            textCounter.text = count.ToString();

            Debug.Log("Meteoriti: " + count);
     }
    }

    public int getNumber()
    {
        return count;
    }

    public void SetNumber()
    {
        count = count - 3;
        textCounter.text = count.ToString();
    }

    public void SetReloadNumber(int reloadCount)
    {
        count = reloadCount;
        textCounter.text = count.ToString();
    }

/* void SetCountText(){
    countText.text= "Count: " + count.ToString();
    } */
}
