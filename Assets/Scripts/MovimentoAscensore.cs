﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public class MovimentoAscensore : MonoBehaviour {

    
    private Vector3 pulsantieraInizio; 
    private Vector3 pulsantieraSu;
    private Vector3 ringhieraInizio; 
    private Vector3 ringhieraSu;
    private Vector3 pedanaInizio; 
    private Vector3 pedanaSu;

    [SerializeField]
    private float velocitaSalita;

    [SerializeField]
    private float velocitaDiscesa;

    [SerializeField]
    private Vector3 salitaMassima = new Vector3();

    /*[SerializeField]
    private Canvas canvas;

    private bool vicino=false; */
    private bool inCorso=false;

    private AudioSource audioSource;
    [SerializeField]
    private AudioClip inMovimento;
    [SerializeField]
    private AudioClip arrivata;

    void Start(){
        GameObject pulsantiera = this.transform.GetChild(0).gameObject;
        GameObject ringhiera = this.transform.GetChild(1).gameObject;
        GameObject pedana = this.transform.GetChild(2).gameObject;

        this.pulsantieraInizio = transform.InverseTransformPoint(pulsantiera.transform.position);
        this.pulsantieraSu = transform.InverseTransformPoint(pulsantiera.transform.position) + salitaMassima;

        this.ringhieraInizio = transform.InverseTransformPoint(ringhiera.transform.position);
        this.ringhieraSu = transform.InverseTransformPoint(ringhiera.transform.position) + salitaMassima;

        this.pedanaInizio = transform.InverseTransformPoint(pedana.transform.position);
        this.pedanaSu = transform.InverseTransformPoint(pedana.transform.position) + salitaMassima;

        //canvas.enabled = false;

        audioSource= GetComponent<AudioSource>(); 
    }

    /* private void OnTriggerEnter(Collider collider){
         if(collider.tag=="Player"){
             SetVicino(true);
             canvas.enabled = true;
         }
     }

     private void OnTriggerExit(Collider collider){
         if(collider.tag=="Player"){
             SetVicino(false);
             canvas.enabled=false; 
         }
     } 


     public void SetVicino(bool boolean) {
             vicino = boolean;
     } */


    void Update()
    {
        if (Time.timeScale == 0 && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        if (Time.timeScale != 0 && !audioSource.isPlaying && audioSource.clip == inMovimento)
        {
            audioSource.Play();
        }
    }

    public void movimento(){
        //if(/*vicino==true && */Input.GetKeyDown(KeyCode.Q) /*&& inCorso==false*/)
       // { 
            GameObject pulsantiera = this.transform.GetChild(0).gameObject;
            GameObject ringhiera = this.transform.GetChild(1).gameObject;
            GameObject pedana = this.transform.GetChild(2).gameObject;

            pulsantiera.transform.DOLocalMove( 
                this.pulsantieraSu, 
                velocitaSalita, 
                false);
        
            ringhiera.transform.DOLocalMove( 
                this.ringhieraSu, 
                velocitaSalita, 
                false);

            pedana.transform.DOLocalMove( 
                this.pedanaSu, 
                velocitaSalita, 
                false);

            inCorso=true;
        
            //canvas.enabled=false;

            PlayInMovimento();

            StartCoroutine(aspettaSu());
        //}
        /*if (inCorso)
        {
            if (transform.InverseTransformPoint(this.transform.GetChild(0).gameObject.transform.position) == pulsantieraSu && audioSource.clip == inMovimento && audioSource.time > 1)
            {
                PlayArrivata();
            }
        }*/
    }

    IEnumerator aspettaSu(){
        yield return new WaitForSeconds(velocitaSalita);
        PlayArrivata();
        yield return new WaitForSeconds(2f);
        tornaGiu();
    }

    void tornaGiu(){

        GameObject pulsantiera = this.transform.GetChild(0).gameObject;
        GameObject ringhiera = this.transform.GetChild(1).gameObject;
        GameObject pedana = this.transform.GetChild(2).gameObject;

        pulsantiera.transform.DOLocalMove( 
            this.pulsantieraInizio, 
            velocitaDiscesa, 
            false);

        ringhiera.transform.DOLocalMove( 
            this.ringhieraInizio, 
            velocitaDiscesa, 
            false);

        pedana.transform.DOLocalMove( 
            this.pedanaInizio, 
            velocitaDiscesa, 
            false);
    
        StartCoroutine(aspettaGiu(false));

        //StartCoroutine(resetInCorso(false));
        PlayInMovimento();
    }

    IEnumerator aspettaGiu(bool boolean){
        yield return new WaitForSeconds(velocitaDiscesa);
        inCorso=boolean;
        PlayArrivata();
        yield return new WaitForSeconds(2f);
        movimento();
    }

    /*IEnumerator resetInCorso(bool boolean){
        yield return new WaitForSeconds(velocitaDiscesa);
        inCorso=boolean;
        PlayArrivata();
    }*/

    private void PlayInMovimento()
    {
        audioSource.clip = inMovimento;
        audioSource.loop = true;
        audioSource.Play();
    }
    private void PlayArrivata()
    {
        audioSource.Stop();
        audioSource.clip = arrivata;
        audioSource.loop = false;
        audioSource.Play();
    }
}