﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WASD : MonoBehaviour
{
    // DA POSIZIONARE SU SAM E PASSARE LA WASD_IMAGE (CHE STA NEL WASD_CANVAS)

    private float waitTime = 5.0f;
    private float timer = 0.0f;
    [SerializeField] private Image WASDimage;
    private GameObject camera_interpolata;
    private bool isenabled = false;
    private Vector3 direzione;


    // Start is called before the first frame update
    void Start()
    {
        WASDimage.enabled = false;
        camera_interpolata = GameObject.FindGameObjectWithTag("CameraInterpolata");
    }
    
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (Input.anyKey)
        {
            timer = 0.0f;
        }

        if (timer >= waitTime)
        {
            direzione = gameObject.transform.position;
            WASDimage.transform.position = new Vector3(direzione.x,direzione.y,direzione.z)-transform.up*1f-camera_interpolata.transform.forward*2f;
            WASDimage.transform.rotation = gameObject.transform.GetComponent<Giocatore>().rotazione_view_dependant_proiettate();
            WASDimage.enabled = true;
            isenabled = true;
            //Debug.Log("WASD!");

        }
        if (isenabled && Input.anyKeyDown)
        {
            WASDimage.enabled = false;
            isenabled = false;
            if (waitTime == 5f) waitTime = 30f;
        }
    }
}
