﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OggettoInteragibile : MonoBehaviour
{
    /*
    Classe da cui ereditare per creare gli script
    di oggetti attivabili/disattivabili.
    In ognuno di essi, implementare la funzione commuta.
    A ogni chiamata, deve far passare lo stato da attivo a disattivo o viceversa.
    */
    public abstract void commuta();
    public abstract void attiva();
    public abstract void disattiva();

}
