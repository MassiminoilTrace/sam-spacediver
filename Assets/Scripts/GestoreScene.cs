﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GestoreScene : MonoBehaviour
{
    public static GestoreScene Instance { set; get; }
    private void Awake()
    {
        Instance = this;
        Carica("Inizio");//Inserire scene da caricare all'avvio
    }

    public void CaricaInizio()
    {
        Carica("Giocatore+camera");
        Carica("LvL1");
        Carica("Corridoi");
    }
    
    public AsyncOperation Carica(string nomeScena)
    {
        if (!SceneManager.GetSceneByName(nomeScena).isLoaded)
        {

            if (nomeScena == "LvL2")
            {
                GameObject obj_musiche = GameObject.FindGameObjectWithTag("GestoreMusiche");
                if (obj_musiche)
                {
                    MusicheDiSottofondo gestoremusiche = obj_musiche.GetComponent<MusicheDiSottofondo>();
                    gestoremusiche.avviaRiproduzione();
                }
            }



            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nomeScena, LoadSceneMode.Additive);
            return asyncLoad;
        }
        else
        {
            return null;
        }
    }
    public AsyncOperation Scarica(string nomeScena)
    {
        if (SceneManager.GetSceneByName(nomeScena).isLoaded)
        {
            AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(nomeScena);
            return asyncLoad;
        }
        else
        {
            return null;
        }
    }
    public bool ECaricata(string nomeScena)
    {
        return SceneManager.GetSceneByName(nomeScena).isLoaded;
    }
}
