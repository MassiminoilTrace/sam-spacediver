﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WASDappearing : MonoBehaviour
{
    private float waitTime = 5.0f;
    private float timer = 0.0f;
    [SerializeField] private Image WASDimage;
    private bool isenabled = false;
    private Vector3[] direzione;
    private Vector3 directionUp;

    public GameObject camera_interpolata;


    // Start is called before the first frame update
    void Start()
    {
        WASDimage.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (Input.anyKeyDown)
        {
            timer = 0.0f;
        }

        if (timer >= waitTime)
        {
            direzione = gameObject.transform.GetComponent<Giocatore>().basi_view_dependant_proiettate();
            directionUp = new Vector3(0, direzione[2].y, direzione[2].z);
            WASDimage.transform.position = gameObject.transform.position;
            WASDimage.transform.rotation = Quaternion.FromToRotation(Vector3.up, direzione[2]);
            WASDimage.enabled = true;
            isenabled = true;

        }
        if (isenabled && Input.anyKeyDown)
        {
            WASDimage.enabled = false;
            isenabled = false;
        }
    }
}
