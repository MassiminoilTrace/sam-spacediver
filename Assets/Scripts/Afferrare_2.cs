﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Afferrare_2 : MonoBehaviour
{
    // Start is called before the first frame update
    // An object need to closer than that distance to be picked up.
    public float pickUpDist = 1.5f;
    public Transform GrabPos;
    private GameObject carriedObject = null;
    private int pickupLayer;
    public bool carring = false;
    public float massa;
    private bool found = false;
    private bool mostraSugg = true;

    [SerializeField]
    private Animator animator;
    private void Start()
    {
        pickupLayer = 1 << LayerMask.NameToLayer("Pickup");
    }

    private void Update()
    {
        if (this.GetComponent<Giocatore>().staPortandoOggetto() && this.GetComponent<Giocatore>().isDead()) Drop();
        if (carring && Input.GetKeyDown(KeyCode.E)) Drop();
        else if (carring) Carring();
        else if (!carring && Input.GetKeyDown(KeyCode.E) && carriedObject != null){
            animationPick();
            this.GetComponent<Giocatore>().setStaPrendendoOggetto(true);
            //Invoke("PickUp", 0.6f);
        }
        //Debug.Log(deathReaction+"death reaction afferrare2");
        if (!carring && !found) Search();


    }

    public void Drop()
    {

        //carriedObject.GetComponent<FakeParent>().StopFakeParent(); // Unparenting
        carriedObject.GetComponent<E_Cubo>().NotGrabbed();
        transform.GetComponent<FallDamage>().SetGrabObj(null);
        carriedObject.GetComponent<E_Cubo>().SetAfferrato(false);
        carriedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        //transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        //transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX;
        //transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
        carring = false;
        found = false;
        carriedObject.GetComponent<Rigidbody>().mass = massa;
        //carriedObject.gameObject.AddComponent(typeof(Rigidbody)); // Gravity and co
        //carriedObject.gameObject.GetComponent<Rigidbody>().mass = 10;
        carriedObject = null; // Hands are free again
        animationDrop();
        if (mostraSugg) mostraSugg = false;
    }

    private void PickUp()
    {
        // Collect every pickups around. Make sure they have a collider and the layer Pickup
        //Vector3 davanti = transform.position * transform.forward;

        /*Collider[] pickups = Physics.OverlapSphere(transform.position+transform.forward*1.5f+transform.up*1.5f, pickUpDist, pickupLayer);
        // Find the closest
        float dist = Mathf.Infinity;
        foreach (Collider v in pickups)
        {
            float newDist = (transform.position - v.transform.position).sqrMagnitude;
            Debug.Log("distanza " + newDist);
            if (newDist < dist)
            {
                carriedObject = v.transform.gameObject;
                carriedObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                dist = newDist;
            }
        }*/
        // Set the box in front of character
        Debug.Log("Preso!");
        if (GetComponent<Giocatore>().GetLivelloAttuale() == 2 && mostraSugg) mostraSugg = true;
        else if (mostraSugg) mostraSugg = false;
        massa = carriedObject.GetComponent<Rigidbody>().mass;
        //transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        carring = true;
        carriedObject.GetComponent<E_Cubo>().IsGrabbed();
        carriedObject.GetComponent<E_Cubo>().SetAfferrato(true);
        //carriedObject.transform.position = GrabPos.position;
        //carriedObject.GetComponent<Rigidbody>().mass=0;
        Carring();
        animationPick();
    }

    private void Search()
    {
        Collider[] pickups = Physics.OverlapSphere(transform.position + transform.forward * 1.5f + transform.up * 1.5f, pickUpDist, pickupLayer);
        // Find the closest
        float dist = Mathf.Infinity;
        GameObject tempCarried = null;
        foreach (Collider v in pickups)
        {
            float newDist = (transform.position - v.transform.position).sqrMagnitude;
            Debug.Log("distanza " + newDist);
            if (newDist < dist)
            {
                tempCarried = v.transform.gameObject;
                dist = newDist;
            }
        }
        if (carriedObject != tempCarried)
        {
            if (carriedObject != null) carriedObject.GetComponent<E_Cubo>().ShowCanvas();
            carriedObject = tempCarried;
            if (carriedObject != null) carriedObject.GetComponent<E_Cubo>().ShowCanvas();
        }
    }

    void animationDrop()
    {
        animator.SetBool("grab", false);
    }
    void animationPick()
    {
        Debug.Log("agg preso");
        found = true;
        animator.SetBool("grab", true);
        
    }


    private void Carring()
    {
        //carriedObject.GetComponent<FakeParent>().SetPosition(transform.position +transform.forward*1.5f);
        //carriedObject.GetComponent<FakeParent>().SetFakeParent(transform);
        carriedObject.GetComponent<Rigidbody>().velocity = (GrabPos.position - carriedObject.transform.position) * 10;
        //carriedObject.transform.SetParent(GrabPos);
        carriedObject.GetComponent<Rigidbody>().mass = 0.1f;
        transform.GetComponent<FallDamage>().SetGrabObj(carriedObject.GetComponent<Collider>());
        carriedObject.transform.rotation = transform.rotation;
        this.GetComponent<Giocatore>().setStaPrendendoOggetto(false);
    }

    public bool MostraSugg()
    {
        return mostraSugg;
    }
}
