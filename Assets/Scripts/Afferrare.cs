﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Afferrare : MonoBehaviour
{
    float Yrot;
    RaycastHit hit;
    GameObject grabbedObj;
    public Transform GrabPos;
    bool isgrabbed = false;


    void Start()
    {

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && Physics.Raycast(transform.position, transform.forward, out hit, 5) && hit.transform.GetComponent<Rigidbody>() && (!isgrabbed))
        {
            grabbedObj = hit.transform.gameObject;
            isgrabbed = true;
            grabbedObj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

        }
        else if (Input.GetKeyDown(KeyCode.E) && (isgrabbed))
        {
            grabbedObj.GetComponent<E_Cubo>().NotGrabbed();
            grabbedObj = null;
            isgrabbed = false;

        }

        if (grabbedObj)
        {
            grabbedObj.GetComponent<Rigidbody>().velocity = (GrabPos.position - grabbedObj.transform.position) * 10;
            grabbedObj.transform.SetParent(GrabPos);
            grabbedObj.GetComponent<E_Cubo>().IsGrabbed();
        }
    }

}
