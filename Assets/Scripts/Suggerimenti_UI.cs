﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suggerimenti_UI : MonoBehaviour
{
    private GameObject player;
    private int LivelloAttuale = 0;
    private int LivelloSuggerimenti = 0;
    private bool RichiestiSuggerimenti = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        LivelloAttuale = player.GetComponent<Giocatore>().GetLivelloAttuale();
        if(RichiestiSuggerimenti) ControllaNuovoLivello();
    }

    public void SetRichiestiSuggerimenti(bool boolean)
    {
        RichiestiSuggerimenti = boolean;
        if (RichiestiSuggerimenti) AttivaSuggerimenti();
    }

    private void AttivaSuggerimenti()
    {
        LivelloSuggerimenti = player.GetComponent<Giocatore>().GetLivelloAttuale();
        string TagLivello = "Sugg" + LivelloSuggerimenti;
        GameObject Canvas = GameObject.FindGameObjectWithTag(TagLivello);
        Canvas.GetComponent<Canvas>().enabled = true;

    }

    private void ControllaNuovoLivello()
    {
        if (LivelloAttuale != LivelloSuggerimenti)
        {
            string TagLivello = "Sugg" + LivelloSuggerimenti;
            GameObject Canvas = GameObject.FindGameObjectWithTag(TagLivello);
            Canvas.GetComponent<Canvas>().enabled = false;
            RichiestiSuggerimenti = false;
        }
    }
}
