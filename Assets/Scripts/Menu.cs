﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;


//Possibile aggiungere un'animazione aggiungendo all'oggetto Menu il Component "Animator", con l'unica accortezza di mettere Update Model = "Unscaled Time"

public class Menu : MonoBehaviour
{
    private GameObject gameMenu;
    private bool menuAperto;
    private string tempLevel;
    private GameObject sam;
    public GameObject pannelloTasti;
    public GameObject pannelloMorte;
    public GameObject pannelloOpzioni;
    public GameObject pannelloLivello;
    public GameObject pannelloSicuro;
    public GameObject pannelloSicuroMorte;
    public GameObject pannelloSuggerimenti;
    public GameObject pannelloLivelloMorte;
    public GameObject bottoneSuggerimenti;
    public GameObject lvlSbagliato;
    public GameObject lvlSbagliatoMorte;
    static Menu instance;
    private GestoreScene gestScene;
    private Dictionary<int, bool> tempDB;
    private float tempHealth;
    private bool riavviante;
    private string scenaAttiva;
    private bool vidAttivo;
    private bool attivaSuggerimenti;
    private bool finito;

    private void Awake()
    {
        gameMenu = transform.GetChild(0).gameObject;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.root.gameObject); //Con questa istruzione rendo "permanente" questo GameObject
        }
        else
        {
            Destroy(transform.root.gameObject);
            return;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        menuAperto = false;
        gameMenu.SetActive(menuAperto);
        pannelloTasti.SetActive(true);
        pannelloMorte.SetActive(false);
        pannelloLivello.SetActive(false);
        pannelloSicuro.SetActive(false);
        FindSam();
        gestScene = GameObject.Find("GestoreScene").GetComponent<GestoreScene>();
        tempDB = new Dictionary<int, bool>();
        riavviante = false;
        scenaAttiva = "";
        attivaSuggerimenti = false;
        finito = false;
        tempHealth = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pannelloMorte.activeSelf && !vidAttivo)
        {
            if (menuAperto)
            {
                CloseMenu();
            }
            else
            {
                OpenMenu();
            }
        }
        if (sam == null)
        {
            FindSam();
        }
    }

    public void Death()
    {
        pannelloTasti.SetActive(false);
        pannelloMorte.SetActive(true);
        if (sam.GetComponent<Giocatore>().GetLivelloAttuale() > 4) StartCoroutine(PopUp(bottoneSuggerimenti));
        OpenMenu();
    }
    public void OpenMenu()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        gameMenu.SetActive(true);
        Time.timeScale = 0;
        if (sam != null) sam.GetComponent<Giocatore>().SetPausa(true);
        menuAperto = gameMenu.activeInHierarchy;


        //Passa basso suono
        GameObject obj_musiche = GameObject.FindGameObjectWithTag("GestoreMusiche");
        if (obj_musiche)
        {
            MusicheDiSottofondo gestoremusiche =  obj_musiche.GetComponent<MusicheDiSottofondo>();
            gestoremusiche.soloBassi(true);
        }
        
    }

    public void CloseMenu()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        gameMenu.SetActive(false);
        Time.timeScale = 1;
        sam.GetComponent<Giocatore>().SetPausa(false);
        if (pannelloMorte.activeSelf)    //Se il menu che viene chiuso è quello di morte visualizza nuovamente quello classico
        {
            pannelloMorte.SetActive(false);
            pannelloTasti.SetActive(true);
        }
        menuAperto = gameMenu.activeInHierarchy;


        //Tolgo passa basso suono
        GameObject obj_musiche = GameObject.FindGameObjectWithTag("GestoreMusiche");
        if (obj_musiche)
        {
            MusicheDiSottofondo gestoremusiche =  obj_musiche.GetComponent<MusicheDiSottofondo>();
            gestoremusiche.soloBassi(false);
        }
    }

    public void CaricaMenuLivello(string daPan)
    {
        switch (daPan)
        {
            case "tasti":
                pannelloTasti.SetActive(false);
                pannelloLivello.SetActive(true);
                break;
            case "morte":
                pannelloMorte.SetActive(false);
                pannelloLivelloMorte.SetActive(true);
                break;
        }
    }

    public void NewTempLevel(string lvl)
    {
        if (lvl != "")
        {
            if (int.Parse(lvl) > 2020 && int.Parse(lvl) < 2029) tempLevel = (int.Parse(lvl) - 2020).ToString();
            else tempLevel = "";
            if (lvlSbagliato.activeSelf) lvlSbagliato.SetActive(false);
            if (lvlSbagliatoMorte.activeSelf) lvlSbagliatoMorte.SetActive(false);
        }
    }

    public void CaricaLivello(InputField input)
    {
        if (tempLevel == "")
        {
            if (pannelloLivello.activeSelf) lvlSbagliato.SetActive(true);
            if (pannelloLivelloMorte.activeSelf) lvlSbagliatoMorte.SetActive(true);
            input.Select();
            input.text = "";
            return;
        }
        riavviante = true;
        tempHealth = (sam.GetComponent<Giocatore>().GetVitaAttuale() <= 0 ? tempHealth : sam.GetComponent<Giocatore>().GetVitaAttuale());
        tempDB = new Dictionary<int, bool>();
        gestScene.Scarica("Giocatore+camera" + scenaAttiva);
        for (int i = 0; i < 9; i++)
        {
            gestScene.Scarica("Giocatore+camera" + i);
            if (gestScene.Scarica("LvL" + i) != null) break;
        }
        gestScene.Scarica("Corridoi");
        Destroy(GameObject.Find("New Game Object"));
        Caricamento();
        FindSam();
        CloseMenu();
        menuAperto = false;
        pannelloTasti.SetActive(true);
        pannelloLivello.SetActive(false);
        pannelloLivelloMorte.SetActive(false);
        input.text = "";
    }

    public void RicominciaLivello() //Script per far ripartire il livello
    {
        riavviante = true;
        tempHealth = sam.GetComponent<Giocatore>().GetVitaInizioLivello();
        gestScene.Scarica("Giocatore+camera" + scenaAttiva);
        for (int i = 0; i < 9; i++)
        {
            if (gestScene.Scarica("LvL" + i) != null)
            {
                tempLevel = i.ToString();
                break;
            }
        }
        gestScene.Scarica("Corridoi");
        Destroy(GameObject.Find("New Game Object"));
        Caricamento();
        FindSam();
        CloseMenu();
        menuAperto = false;
        if (attivaSuggerimenti)
        {
            StartCoroutine(AttivaSuggerimenti());
        }
    }

    public void Options()
    {
        pannelloTasti.SetActive(false);
        pannelloOpzioni.SetActive(true);
    }
    public void AttivaMusica(GameObject toggle)
    {
        switch (toggle.GetComponent<Toggle>().isOn)
        {
            case true:
                GameObject.FindGameObjectWithTag("GestoreMusiche").GetComponent<MusicheDiSottofondo>().avviaRiproduzione();
                break;
            case false:
                GameObject.FindGameObjectWithTag("GestoreMusiche").GetComponent<MusicheDiSottofondo>().fermaRiproduzione();
                break;
        }
    }

    public void Sicuro(string daPan)
    {
        switch (daPan)
        {
            case "tasti":
                pannelloTasti.SetActive(false);
                pannelloSicuro.SetActive(true);
                break;
            case "morte":
                pannelloMorte.SetActive(false);
                pannelloSicuroMorte.SetActive(true);
                break;
            case "suggerimenti":
                pannelloMorte.SetActive(false);
                pannelloSuggerimenti.SetActive(true);
                break;
        }
    }

    public void Indietro(string daPan)
    {
        switch (daPan)
        {
            case "sicuro":
                pannelloSicuro.SetActive(false);
                pannelloTasti.SetActive(true);
                break;
            case "opzioni":
                pannelloOpzioni.SetActive(false);
                pannelloTasti.SetActive(true);
                break;
            case "carica":
                pannelloLivello.SetActive(false);
                pannelloTasti.SetActive(true);
                if (lvlSbagliato.activeSelf) lvlSbagliato.SetActive(false);
                break;
            case "caricaMorte":
                pannelloLivelloMorte.SetActive(false);
                pannelloMorte.SetActive(true);
                if (lvlSbagliatoMorte.activeSelf) lvlSbagliatoMorte.SetActive(false);
                break;
            case "morte":
                pannelloSicuroMorte.SetActive(false);
                pannelloMorte.SetActive(true);
                break;
            case "suggerimentiSi":
                attivaSuggerimenti = true;
                pannelloSuggerimenti.SetActive(false);
                pannelloMorte.SetActive(true);
                bottoneSuggerimenti.SetActive(false);
                break;
            case "suggerimentiNo":
                pannelloSuggerimenti.SetActive(false);
                pannelloMorte.SetActive(true);
                bottoneSuggerimenti.SetActive(false);
                break;
        }
    }

    public void Esci()
    {
        gestScene.Scarica("Giocatore+camera" + scenaAttiva);
        for (int i = 0; i < 9; i++)
        {
            if (gestScene.Scarica("LvL" + i) != null)
            {
                tempLevel = i.ToString();
                break;
            }
        }
        gestScene.Scarica("Corridoi");
        gestScene.Carica("Inizio");
    }

    public void Finale()
    {
        gestScene.Scarica("Giocatore+camera" + scenaAttiva);
        for (int i = 0; i < 9; i++)
        {
            if (gestScene.Scarica("LvL" + i) != null)
            {
                tempLevel = i.ToString();
                break;
            }
        }
        gestScene.Scarica("Corridoi");
        finito = true;
        gestScene.Carica("Inizio");
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void FindSam()
    {
        if (sam != null)
        {
            Destroy(sam);
        }
        sam = GameObject.FindGameObjectWithTag("Player");
    }

    private void Caricamento()  //Dubbio sul caricamento: è meglio mettere nell'if Carica() e rischiare di avere un errore perchè non trova la scena oppure metterci una funzione che cerca la scena senza caricarla?
    {
        if (gestScene.Carica("Giocatore+camera" + (tempLevel == "1" ? "" : tempLevel)) != null) //tempLevel == "1" ? "" : tempLevel --> Questo praticamente ritorna "" se tempLevel è uguale a 1 e ritorna il valore di tempLevel se è diverso da 1
        {
            gestScene.Carica("LvL" + tempLevel);
            scenaAttiva = tempLevel;
        }
        else
        {
            gestScene.Carica("Giocatore+camera");
            gestScene.Carica("LvL1");
            scenaAttiva = "";
            Debug.Log("Gestore+camera" + tempLevel + " non trovato :(");
        }
        //gestScene.Carica("Giocatore+camera");
        //gestScene.Carica("LvL" + tempLevel);
        gestScene.Carica("Corridoi");
    }

    public void SetMeteoriti(Dictionary<int, bool> tempDataBase)
    {
        tempDB = new Dictionary<int, bool>(tempDataBase);
    }

    public Dictionary<int, bool> GetMeteoritiDB()
    {
        riavviante = false;
        GameObject.FindGameObjectWithTag("Player").GetComponent<CalcoloMeteoriti>().SetReloadNumber(GetMeteoritiNum());
        return tempDB;
    }

    public int GetMeteoritiNum()
    {
        int i = 0;
        foreach (bool b in tempDB.Values) if (b) i++; 
        return i;
    }

    public bool Riavviante()
    {
        return riavviante;
    }

    public void VidAttivo(bool att)
    {
        vidAttivo = att;
    }

    IEnumerator PopUp(GameObject go)
    {
        yield return new WaitForSecondsRealtime(0.5f);
        go.SetActive(true);
    }

    IEnumerator AttivaSuggerimenti()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        sam.GetComponent<Suggerimenti_UI>().SetRichiestiSuggerimenti(true);
        attivaSuggerimenti = false;
    }

    public bool GetFinito()
    {
        return finito;
    }

    public float GetHealth()
    {
        return tempHealth;
    }
}
