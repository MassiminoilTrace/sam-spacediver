﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridoioCaricamento : MonoBehaviour
{
    [SerializeField]
    private TriggerCaricamento rilevatoreLivelloPrecedente;
    [SerializeField]
    private TriggerCaricamento rilevatoreLivelloSuccessivo;

    //Porte da commutare
    // [SerializeField]
    // private Porta portaPrecedente;
    // [SerializeField]
    // private Porta portaSuccessiva;
    [SerializeField]
    private string nomeScenaPrecedente;
    [SerializeField]
    private string nomeScenaSuccessiva;

    void Start()
    {
        this.rilevatoreLivelloPrecedente.impostaScenaDaScaricare(nomeScenaSuccessiva);
        if (nomeScenaPrecedente != "LvL1") this.rilevatoreLivelloPrecedente.impostaScenaDaCaricare(nomeScenaPrecedente);    //ORA IL BUCCHINARO NON TORNA AL LVL1

        if (nomeScenaSuccessiva != "LvL1") this.rilevatoreLivelloSuccessivo.impostaScenaDaCaricare(nomeScenaSuccessiva);    //ORA IL BUCCHINARO NON TORNA AL LVL1
        this.rilevatoreLivelloSuccessivo.impostaScenaDaScaricare(nomeScenaPrecedente);
    }

}
