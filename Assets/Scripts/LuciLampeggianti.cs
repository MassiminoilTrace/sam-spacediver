﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuciLampeggianti : MonoBehaviour
{
    [SerializeField] Light RedLightDX;
    [SerializeField] Light RedLightSX;

    private Vector3 redDX_temp;
    private Vector3 redSX_temp;

    [SerializeField] int speed;

    private AudioSource audioSource;
    private bool stopped;
    private bool videoOver;
    [SerializeField]
    private AudioClip boom;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (stopped != true) stopped = false;
        videoOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        redDX_temp.y += -speed * Time.deltaTime;
        redSX_temp.y += speed * Time.deltaTime;

        RedLightDX.transform.eulerAngles = redDX_temp;
        RedLightSX.transform.eulerAngles = redSX_temp;
        if (Time.timeScale == 0 && audioSource.isPlaying && videoOver)
        {
            audioSource.Stop();
        }
        if (Time.timeScale == 1 && !stopped && !audioSource.isPlaying)
        {
            audioSource.Play();
        }
        if (!transform.GetChild(0).GetChild(0).GetComponent<Light>().enabled  && audioSource.isPlaying)
        {
            audioSource.Stop();
            stopped = true;
        }
    }

    public void PlayBoom()
    {
        audioSource.PlayOneShot(boom);
    }

    public void PlaySiren()
    {
        audioSource.Play();
    }

    public void VideoOver()
    {
        videoOver = true;
    }
}
