﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PrimoBottone : MonoBehaviour
{
    public Porta portaDaAprire;

    private GameObject player;
    private Collider colliderPlayer;

    private bool vicino = false; //aggiungere canvas 

    private bool giaAperta = false; //impedisce di premere il pulsante più volte

    [SerializeField]
    private Canvas canvas; //aggiunta canva

    private bool SecondoBottone = false;

    //AUDIO
    private AudioSource audioSource;
    private Vector3 posizioneSu;
    private Vector3 posizioneGiu;

    private void Start()    //AUDIO
    {
        audioSource = GetComponent<AudioSource>();
        canvas.enabled = false;

        //per il movimento del bottone 
        GameObject bottone = this.transform.GetChild(1).gameObject;
        this.posizioneSu = transform.InverseTransformPoint(bottone.transform.position);
        this.posizioneGiu = transform.InverseTransformPoint(bottone.transform.position) + new Vector3(0, -0.01f, 0);
    }

    public void SetSecondoBottone(bool boolean)
    {
        SecondoBottone = boolean;
    }
    
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
           // Debug.Log("In trigger");
            SetVicino(true);
            //Debug.Log("Valore vicino: " + vicino);
            
            if(giaAperta==false){
            canvas.enabled = true;
            }
        }
    }

     private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            
            //Debug.Log("Valore vicino: " + vicino);
            //Debug.Log("Fuori trigger");
            SetVicino(false);
            //Debug.Log("Valore vicino: " + vicino);
            canvas.enabled = false;
        }
    }

    public void SetVicino(bool boolean)
    {
        vicino = boolean;
        //Debug.Log("cambio valore vicino");
    }

    // Update is called once per frame
    void Update()
    {
        if (vicino==true && Input.GetKeyDown(KeyCode.E) && giaAperta==false && SecondoBottone)
        {
            //movimento bottone 
            GameObject bottone = this.transform.GetChild(1).gameObject;
            bottone.transform.DOLocalMove(
            this.posizioneGiu,
            0.3f,
            false);
            
            StartCoroutine(tornaSu()); //fine movimento del bottone su 

            portaDaAprire.attiva();
            giaAperta=true;

            canvas.enabled = false;
            audioSource.Play();

            //Debug.Log("Premuto Bottone E");
            //audioSource.Play();
        }
        if(!SecondoBottone && giaAperta)
        {
            portaDaAprire.disattiva();
            giaAperta = false;
            
        }
    }
        
        //ritorno su del bottone dopo un po' di tempo 
        IEnumerator tornaSu(){
            yield return new WaitForSeconds(1f);
            GameObject bottone= this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneSu, 
            0.2f,
            false);
        }

}
