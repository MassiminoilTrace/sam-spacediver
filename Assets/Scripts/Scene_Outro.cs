﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;

public class Scene_Outro : MonoBehaviour
{
    public VideoPlayer vid;
    public Canvas canvas;

    void Start()
    {
        canvas.enabled = false;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            GameObject obj_musiche = GameObject.FindGameObjectWithTag("GestoreMusiche");
            if (obj_musiche)
            {
                MusicheDiSottofondo gestoremusiche = obj_musiche.GetComponent<MusicheDiSottofondo>();
                gestoremusiche.fermaRiproduzione();
            }

            canvas.GetComponent<CanvasGroup>().DOFade(1f, 2f);
            canvas.enabled = true;
            StartCoroutine(PlayVideo());
        }
    }

    IEnumerator PlayVideo()
    {
        yield return new WaitForSecondsRealtime(2f);
        Time.timeScale = 0;
        GameObject.FindGameObjectWithTag("Player").GetComponent<Giocatore>().SetPausa(true);
        GameObject.Find("Menu").GetComponent<Menu>().VidAttivo(true);
        vid.Play();
        vid.loopPointReached += CheckOver;
    }

    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        StartCoroutine(Finisc());
    }

    IEnumerator Finisc()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        Time.timeScale = 1;
        GameObject.Find("Menu").GetComponent<Menu>().VidAttivo(false);
        GameObject.Find("Menu").GetComponent<Menu>().Finale();
    }

}