﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWithTimeScale : MonoBehaviour
{
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0 && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        if (Time.timeScale != 0 && !audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }
}
