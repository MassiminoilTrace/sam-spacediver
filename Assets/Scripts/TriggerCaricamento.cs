﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerCaricamento : MonoBehaviour
{
    [SerializeField]
    private bool carica_flag;
    [SerializeField]
    private bool scarica_flag;
    [SerializeField]
    private string nomeScenaDaCaricare;
    [SerializeField]
    private string nomeScenaDaScaricare;


    [SerializeField]
    private Porta portaPerLivelloDaCaricare;
    [SerializeField]
    private Porta portaPerLivelloDaScaricare;



    public void impostaScenaDaCaricare(string s)
    {
        this.nomeScenaDaCaricare = s;
    }
    public void impostaScenaDaScaricare(string s)
    {
        this.nomeScenaDaScaricare = s;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Player" || collider.isTrigger)
        {
            return;
        }
        if (carica_flag)
        {
            carica();
        }
        if (scarica_flag)
        {
            scarica();
        }
    }

    public void carica()
    {
        if (nomeScenaDaCaricare!=null)
        {
            StartCoroutine(CaricaScenaAsincrona(nomeScenaDaCaricare));
        }
    }
    public void scarica()
    {
        if (nomeScenaDaScaricare!=null)
        {
            StartCoroutine(ScaricaScenaAsincrona(nomeScenaDaScaricare));
        }
    }
    IEnumerator ScaricaScenaAsincrona(string nomeScena)
    {
        this.portaPerLivelloDaScaricare.disattiva();
        yield return new WaitForSeconds(.31f);

        AsyncOperation asyncLoad = GestoreScene.Instance.Scarica(nomeScena);
        if (asyncLoad==null)
        {
            yield break;
        }
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    IEnumerator CaricaScenaAsincrona(string nomeScena)
    {
        AsyncOperation asyncLoad = GestoreScene.Instance.Carica(nomeScena);
        if (asyncLoad==null)
        {
            yield break;
        }
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        //this.portaPerLivelloDaCaricare.attiva();  HO DISATTIVATO QUESTO PER EVITARE CHE SAM TORNI INDIETRO
    }

    // void Update()
    // {
    //     Debug.Log("Caricamento di "+nomeScenaDaCaricare);
    //     Debug.Log("Scaricamento di "+nomeScenaDaScaricare);

    //     if (GestoreScene.Instance.ECaricata(nomeScenaDaCaricare))
    //     {
    //         this.portaPerLivelloDaCaricare.attiva();
    //     }
    //     else
    //     {
    //         this.portaPerLivelloDaScaricare.attiva();
    //     }
    // }
}
