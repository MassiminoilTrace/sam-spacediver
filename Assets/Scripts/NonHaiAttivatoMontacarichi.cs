﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NonHaiAttivatoMontacarichi : MonoBehaviour
{
    private Canvas Canvas_Sugg8;
    private bool triggerAttivo = false;
    [SerializeField] Image BottoneMontacarichi;
    
    void Start()
    {
        Canvas_Sugg8 = GameObject.FindGameObjectWithTag("Sugg8").GetComponent<Canvas>();
        this.GetComponent<Image>().enabled = false;
        BottoneMontacarichi.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Canvas_Sugg8.enabled)
        {
            triggerAttivo = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggerAttivo)
        {
            this.GetComponent<Image>().enabled = true;
            BottoneMontacarichi.enabled = true;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (triggerAttivo)
        {
            this.GetComponent<Image>().enabled = false;
            BottoneMontacarichi.enabled = false;
        }

    }
}
