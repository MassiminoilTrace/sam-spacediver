﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Scene_Intro : MonoBehaviour
{
    public VideoPlayer vid;
    public Canvas canva;
    private GameObject[] luci;
    private AudioSource audioSource;
    private bool colpito;
    private bool sirene;

    void Start() 
    {
        audioSource = GetComponent<AudioSource>();
        colpito = false;
        sirene = false;
    }

    private void Update()
    {
        if (vid.time >= 6f && !colpito)
        {
            PlayBoom();
            colpito = true;
        }
        if (vid.time >= 7.6f && !sirene)
        {
            PlaySiren();
            sirene = true;
        }
    }

    public void StartVideo()
    {
        Time.timeScale = 0;
        GameObject.Find("Menu").GetComponent<Menu>().VidAttivo(true);
        vid.loopPointReached += CheckOver;
        StartCoroutine(CaricaLivello());
        StartCoroutine(PausaPlayer());
    }

    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        canva.enabled = false;
        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("Player").GetComponent<Giocatore>().SetPausa(false);
        GameObject.Find("Menu").GetComponent<Menu>().VidAttivo(false);
        if (luci != null) foreach (GameObject go in luci) go.GetComponent<LuciLampeggianti>().VideoOver();
        GameObject.Find("GestoreScene").GetComponent<GestoreScene>().Scarica("Inizio");
    }

    private void PlayBoom()
    {
        if (luci != null) luci[0].GetComponent<LuciLampeggianti>().PlayBoom();
        else canva.GetComponent<AudioSource>().PlayOneShot(canva.GetComponent<AudioSource>().clip);
        StartCoroutine(FadeOut(1f));
    }

    private void PlaySiren()
    {
        if(luci != null) foreach (GameObject go in luci) go.GetComponent<LuciLampeggianti>().PlaySiren();
    }

    IEnumerator PausaPlayer()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        while (luci == null)
        {
            yield return new WaitForSecondsRealtime(0.5f);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Giocatore>().SetPausa(true);
            luci = GameObject.FindGameObjectsWithTag("LuceLampeggiante");
        }
    }

    IEnumerator FadeOut(float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }

    IEnumerator CaricaLivello()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        GameObject.Find("GestoreScene").GetComponent<GestoreScene>().CaricaInizio();
    }

}