﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Grabbing : MonoBehaviour
{
    // DA POSIZIONARE SU SAM


    public Transform player;
    //public Transform playerCam;
    bool hasPlayer = false;
    bool beingCarried = false;
    private bool touched = false;
    private float massa;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        
        float dist = Vector3.Distance(gameObject.transform.position, player.position);
        
        if (dist <= 2f)
        {
            hasPlayer = true;
            
        }
        else
        {
            hasPlayer = false;
        }
        if (hasPlayer && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Preso");
            GetComponent<Rigidbody>().isKinematic = true;
            gameObject.transform.SetParent(player);
            beingCarried = true;
            hasPlayer = false;
            
        }
        
        if (beingCarried)
        {
            if (touched)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
                touched = false;
            }
            
            else if (Input.GetKeyDown(KeyCode.E))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }
        }
        
    }
      
    void OnTriggerEnter()
    {
        if (beingCarried)
        {
            touched = true;
        }
    }

}