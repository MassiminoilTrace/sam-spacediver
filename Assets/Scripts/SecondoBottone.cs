﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SecondoBottone : MonoBehaviour
{
    private int vecchio_conteggio_oggetti_in_collisione = 0;
    private int conteggio_oggetti_in_collisione = 0;
    private bool isActive = false;
    public GameObject PrimoBottone;

    //AUDIO
    private AudioSource audioSource;
    private Vector3 posizioneSu;
    private Vector3 posizioneGiu;

    private void Start()    //AUDIO
    {
        audioSource = GetComponent<AudioSource>();
        GameObject bottone = this.transform.GetChild(1).gameObject;
        this.posizioneSu = transform.InverseTransformPoint(bottone.transform.position);
        this.posizioneGiu = transform.InverseTransformPoint(bottone.transform.position) + new Vector3(0, -0.01f, 0);
        disattiva();
    }

    public bool IsActive()
    {
        return isActive;
    }
  
    public  void attiva()
    {
        
        //movimento pulsante quando premuto
        GameObject bottone = this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneGiu,
            0.3f,
            false);

        //Dico al primo bottone che sono attivo
        PrimoBottone.GetComponent<PrimoBottone>().SetSecondoBottone(true);
        isActive = true;

        //AUDIO
        audioSource.Play();
    }
    public void disattiva()
    {
        

        //Da inserire il comando per eseguire l'animazione
        // di ri-alzamento del pulsante
        GameObject bottone = this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneSu,
            0.3f,
            false);

        //Dico al primo bottone che non sono attivo
        PrimoBottone.GetComponent<PrimoBottone>().SetSecondoBottone(false);
        isActive = false;
    }

    private void aggiornaConteggio()
    {
        if (
            conteggio_oggetti_in_collisione == 0
            &&
            vecchio_conteggio_oggetti_in_collisione > 0
        )
        {
            //Se prima c'erano oggetti e ora non più
            this.disattiva();
        }
        else if (
            conteggio_oggetti_in_collisione > 0
            &&
            vecchio_conteggio_oggetti_in_collisione == 0
        )
        {
            //Se ora ci sono oggetti sopra e prima no
            this.attiva();
        }
        vecchio_conteggio_oggetti_in_collisione = conteggio_oggetti_in_collisione;

    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Player" && collider.tag != "PickUp")
        {
            return;
        }
        this.conteggio_oggetti_in_collisione++;
        this.aggiornaConteggio();
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.tag != "Player" && collider.tag != "PickUp")
        {
            return;
        }
        this.conteggio_oggetti_in_collisione--;
        this.aggiornaConteggio();
    }

}
