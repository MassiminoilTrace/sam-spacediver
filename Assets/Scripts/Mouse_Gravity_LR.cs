﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouse_Gravity_LR : MonoBehaviour
{
    // DA POSIZIONARE SUL CANVAS_GRAVITY

    private float waitTime = 7.0f;
    private float timer = 0.0f;
    private bool startTimer = false;
    private GameObject cameraInterpolata;
    private Transform player;
    private int click = 0;


    private int LivelloAttuale;


    // Start is called before the first frame update
    void Start()
    {
        cameraInterpolata = GameObject.FindGameObjectWithTag("CameraInterpolata");
        this.GetComponent<Canvas>().enabled = false;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        LivelloAttuale = player.GetComponent<Giocatore>().GetLivelloAttuale();
    }
    
    // Update is called once per frame
    void Update()
    {
        LivelloAttuale = player.GetComponent<Giocatore>().GetLivelloAttuale();

        
        if (startTimer)
        {
            timer += Time.deltaTime;
        }

        if (LivelloAttuale==2)
        {

            this.GetComponent<Canvas>().enabled = true;
            this.GetComponent<Canvas>().transform.position = player.position;// + Physics.gravity.normalized * -1.5f;
            this.GetComponent<Canvas>().transform.rotation = cameraInterpolata.transform.rotation;
            startTimer = true;

        }
        if (this.GetComponent<Canvas>().enabled && (timer>=waitTime || click>0))
        {
            this.GetComponent<Canvas>().enabled = false;
            startTimer = false;
        }

        if (Input.GetButtonDown("PulsanteGravDestra") || Input.GetButtonDown("PulsanteGravSinistra")) click++;
        
    }

    
}
