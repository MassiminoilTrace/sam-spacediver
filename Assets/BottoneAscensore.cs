﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 


public class BottoneAscensore : MonoBehaviour
{
   public MovimentoAscensore ascensoreDaAttivare;

    private GameObject player;
    private Collider colliderPlayer;

    private bool vicino = false; //aggiungere canvas 

    private Vector3 posizioneSu; //movimento bottone 
    private Vector3 posizioneGiu; //movimento bottone 

    private bool giaAvviato= false;

private AudioSource audioSource;

    [SerializeField]
    private Canvas canvas; //aggiunta canva 

    void Start()
    {
        audioSource = transform.GetChild(0).GetComponent<AudioSource>(); 
        canvas.enabled = false;

        //per il movimento del bottone 
        GameObject bottone= this.transform.GetChild(1).gameObject;
        this.posizioneSu= transform.InverseTransformPoint(bottone.transform.position);
        this.posizioneGiu= transform.InverseTransformPoint(bottone.transform.position) + new Vector3(0, -0.01f, 0);
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
           // Debug.Log("In trigger");
            SetVicino(true);
            //Debug.Log("Valore vicino: " + vicino);
            
            if(giaAvviato==false){
            canvas.enabled = true;
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            
            //Debug.Log("Valore vicino: " + vicino);
            //Debug.Log("Fuori trigger");
            SetVicino(false);
            //Debug.Log("Valore vicino: " + vicino);
            canvas.enabled = false;
        }
    }

    public void SetVicino(bool boolean)
    {
        vicino = boolean;
        //Debug.Log("cambio valore vicino");
    }

     void Update()
    {
        if (vicino==true && Input.GetKeyDown(KeyCode.Q) && giaAvviato==false)
        {
            //movimento bottone 
            GameObject bottone = this.transform.GetChild(1).gameObject;
            bottone.transform.DOLocalMove(
            this.posizioneGiu,
            0.3f,
            false);
            
            StartCoroutine(tornaSu()); //fine movimento del bottone su 

            ascensoreDaAttivare.movimento();
            giaAvviato=true;

            canvas.enabled = false;

            //Debug.Log("Premuto Bottone E");
            audioSource.Play();
        }
        }
        
        //ritorno su del bottone dopo un po' di tempo 
        IEnumerator tornaSu(){
            yield return new WaitForSeconds(1f);
            GameObject bottone= this.transform.GetChild(1).gameObject;
        bottone.transform.DOLocalMove(
            this.posizioneSu, 
            0.2f,
            false);
        }
}
